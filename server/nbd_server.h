//
// Created by jiash on 05/01/2022.
//

#ifndef LIB_NBD_NBD_SERVER_H
#define LIB_NBD_NBD_SERVER_H

#include <stdint.h>
/**
 * Types of virtuatlization
 **/
typedef enum {
    VIRT_NONE = 0,    /**< No virtualization */
    VIRT_IPLIT,    /**< Literal IP address as part of the filename */
    VIRT_IPHASH,    /**< Replacing all dots in an ip address by a / before
			     doing the same as in IPLIT */
    VIRT_CIDR,    /**< Every subnet in its own directory */
} VIRT_STYLE;

/**
 * Variables associated with a server.
 **/
typedef struct {
    char *exportname;    /**< (unprocessed) filename of the file we're exporting */
    char *net_port;
    uint64_t expected_size; /**< size of the exported file as it was told to us through configuration */
    char *listenaddr;   /**< The IP address we're listening on */
    char *authname;      /**< filename of the authorization file */
    int flags;           /**< flags associated with this exported file */
    VIRT_STYLE virtstyle;/**< The style of virtualization, if any */
    uint8_t cidrlen;     /**< The length of the mask when we use CIDR-style virtualization */
    char *servename;    /**< name of the export as selected by nbd-client */
    int max_connections; /**< maximum number of opened connections */
    int numclients;      /**< number of clients connected to this export */
    char *cowdir;         /**< directory for copy-on-write diff files. */
} SERVER;

int nbd_server(SERVER *serve);

#endif //LIB_NBD_NBD_SERVER_H
