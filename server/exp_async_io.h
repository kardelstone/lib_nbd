//
// Created by jiashu on 5/27/19.
//

#ifndef LIB_NBD_EXP_ASYNC_IO_H
#define LIB_NBD_EXP_ASYNC_IO_H

#include <stdint.h>

struct exp_async_data;
//Submission Queue Entry
struct expand_sqe {
    uint64_t cid;
    uint32_t opcode;
    uint32_t len;
    uint64_t offset;
    uint64_t user_data;
};

struct expand_event {
    uint64_t user_data;
    struct expand_sqe *obj;
    uint64_t result;
};

struct exp_async_io {
    struct exp_async_data *private;

    int (*open)(struct exp_async_io *this, const char *device_name);

    int (*close)(struct exp_async_io *this);

    /**
     * Get the device/file size.
     * @param this
     * @return The size of device
     */
    uint64_t (*get_device_size)(struct exp_async_io *this);

    int (*submit_io)(struct exp_async_io *this, struct expand_sqe *sqe);

    int (*get_event)(struct exp_async_io *this, struct expand_event *events);

    /**
     * Flush data to a client
     * @param this
     * @return 0 on success, nonzero on failure
     */
    int (*flush)(struct exp_async_io *this);
};

extern const struct exp_async_io expand_standard;

#endif //LIB_NBD_EXPAND_IO_H
